﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {
    
    public AudioSource backGroundSource;
    private AudioController instance;

    public AudioClip audioJump;
    public AudioClip audioDie;

    public GameObject pausePopup;
    public GameObject settingPopup;
    // Use this for initialization
    private void Start () {
        backGroundSource.Play();
	}

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
    }

    public void PauseAudioClick()
    {
        backGroundSource.volume = 0;
        pausePopup.SetActive(false);
        settingPopup.SetActive(false);
        Time.timeScale = 1;
    }

    public void PlayAudio(AudioClip clip)
    {
        backGroundSource.PlayOneShot(clip);
    }

    public void PlayAudioClick()
    {
        backGroundSource.volume = 1;
        pausePopup.SetActive(false);
        settingPopup.SetActive(false);
        Time.timeScale = 1;
    }

}
