﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawController : MonoBehaviour
{

    public float speedRound;
    public float speedMove;
    public GameObject sawChildren;
    private SpriteRenderer sawRenderer;

    private void Start()
    {
        sawRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(sawRenderer.isVisible)
        {
            sawChildren.transform.Rotate(0, 0, speedRound * Time.deltaTime);
            transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        }
        if(transform.position.y<-5.5f)
        {
            Destroy(gameObject);
        }
    }

}
