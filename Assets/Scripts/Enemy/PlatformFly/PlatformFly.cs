﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlatformFly : MonoBehaviour
{
    private SpriteRenderer platformRenderer;
    public float speedFly;
    public bool checkMove;
    public float deltaY;

    //[SerializeField] PlayerController playerController;
    private void Start()
    {
        checkMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (checkMove == true)
        {
            transform.DOMoveY(deltaY, 1);
        }
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            checkMove = true;
            //playerController.checkJump = false;
            deltaY = transform.position.y + 1.7f;
        }
    }
}
