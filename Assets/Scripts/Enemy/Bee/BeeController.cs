﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeController : MonoBehaviour
{

    public Animator anim;
    private SpriteRenderer beeRenderer;
    public float timeWait;
    public float speedMove;
    bool isMove;


    // Use this for initialization
    private void Start()
    {
        beeRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (beeRenderer.isVisible)
        {
            BeeMove();
        }

    }
    private void BeeMove()
    {
        StartCoroutine(play());
        if (transform.position.y < -5f)
        {
            Destroy(gameObject);
        }
    }
    private IEnumerator play()
    {
        yield return new WaitForSeconds(1);
        anim.SetBool("Play", isMove);
        transform.Translate(new Vector2(-0.7f, -1) * Time.deltaTime * speedMove);
    }
}
