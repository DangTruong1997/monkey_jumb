﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NhenController : MonoBehaviour
{
    private SceneGameController sceneGameController;
    public Animator anim;
    private SpriteRenderer nhenRenderer;
    public float speedMove;
    private bool isTurned;
    // Use this for initialization
    private void Start()
    {
        sceneGameController = FindObjectOfType<SceneGameController>();
        isTurned = false;
        nhenRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if(nhenRenderer.isVisible)
        {
            anim.Play("NhenMove");
            transform.Translate(Vector3.left * Time.deltaTime * speedMove);
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Box"&&!isTurned)
        {
            isTurned = true;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y+180, transform.eulerAngles.z);
        }
    }
    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Box" )
        {
            isTurned = false;
        }
    }
    public void NhenDie()
    {
        Destroy(gameObject);
        sceneGameController.score += 10;
    }
    

}
