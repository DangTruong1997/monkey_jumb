﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FrogController : MonoBehaviour {

    public Animator anim;
    private Rigidbody2D rigid;
    private SpriteRenderer FrogRenderer;
    public float TimeWait;
    public float SpeedJump;
    public bool checkJump;
    private void Start()
    {
        checkJump = false;
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        FrogRenderer = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update () {
        if(transform.position.y<-5f)
        {
            Destroy(gameObject);
        }
		if(FrogRenderer.isVisible && checkJump == false)
        {
            StartCoroutine(FrogPlay());
            checkJump = true;
        }
        if(transform.position.y<-5f)
        {
            Destroy(gameObject);
        }
	}
    IEnumerator FrogPlay()
    {
        anim.Play("frogstand");
        yield return new WaitForSeconds(TimeWait);
        anim.Play("frogplay");
        rigid.AddForce(new Vector3(-.5f, 1, 0) * SpeedJump * 100);
       
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Map")
        {
            checkJump = false;
        }
    }
}
