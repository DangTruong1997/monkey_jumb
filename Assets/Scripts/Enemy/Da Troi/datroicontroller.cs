﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class datroicontroller : MonoBehaviour
{

    private PlayerController playerController;

    public float speedDaTroiMove;
    public float speedMove;
    private SpriteRenderer daTroiRenderer;
    public bool checkEnterPlayer;
    public bool checkEnd;

    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        speedDaTroiMove = speedMove;
        checkEnterPlayer = false;
        daTroiRenderer = GetComponent<SpriteRenderer>();
        checkEnd = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (daTroiRenderer.isVisible && checkEnterPlayer == true)
        {
            speedDaTroiMove = playerController.speedMovePlayer;
            transform.Translate(Vector3.right * speedDaTroiMove);
        }
    }


    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Map")
        {
            print("va cham");
            speedDaTroiMove = 0;
            checkEnterPlayer = false;
            checkEnd = true;
        }
    }
}
