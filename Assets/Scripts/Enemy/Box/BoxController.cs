﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {

    private SceneGameController sceneGameController;

    private void Start()
    {
        sceneGameController = FindObjectOfType<SceneGameController>();
    }

    public void BoxDie()
    {
        gameObject.SetActive(false);
        sceneGameController.score += 3;
    }
}
