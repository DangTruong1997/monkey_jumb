﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Animator anim;
    private PlayerController instance;
    private BoxCollider2D boxPlayer;
    public bool checkJump, checkDie, checkEnterDaTroi;
    public float speedMovePlayer;
    public float speedMove, speedJump, speedDie;
    public float speedMoveSwamp;
    public float speedMoveBox;

    public GameObject baoVe;
    public float timeWaitBaove;
    public bool itemBaove;

    private Rigidbody2D rigid;
    public float numberPlayer;

    [SerializeField] SceneGameController sceneGameController;
    [SerializeField] AudioController audioController;

    

    // Use this for initialization
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
        rigid = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        boxPlayer = GetComponent<BoxCollider2D>();
    }

    private void OnEnable()
    {
        boxPlayer.isTrigger = false;
        checkJump = false;
        checkDie = false;
        baoVe.SetActive(false);
        checkEnterDaTroi = false;
        itemBaove = false;
        transform.position = new Vector3(-3, 0.04f);
    }

    // Update is called once per frame
    private void Update()
    {
        numberPlayer = PlayerPrefs.GetFloat("NumberPlayer");
        if (!checkDie)
        {
            transform.Translate(Vector3.right * speedMovePlayer);

            if ((checkJump == false && Input.GetKeyDown(KeyCode.Space) || checkJump == false && Input.GetMouseButtonDown(0)))
            {
                PlayerJump();
            }
            if ((checkJump == false && checkEnterDaTroi == false) && Mathf.RoundToInt(rigid.velocity.y) == 0)
            {
                PlayerWalk();
            }
        }
        if (transform.position.y < -5.5f)
        {
            gameObject.SetActive(false);

        }

    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if ((coll.gameObject.layer == LayerMask.NameToLayer("Enemy") && itemBaove == false) || (coll.gameObject.layer == LayerMask.NameToLayer("EnemyNuoc") && itemBaove == false))
        {
            PlayerDie();
        }


        if ((coll.gameObject.tag == "Map" || coll.gameObject.tag == "Box") && Mathf.RoundToInt(rigid.velocity.y) == 0)
        {
            PlayerWalk();
            checkJump = false;
            speedMovePlayer = speedMove;
        }

        //enter da troi
        if (coll.gameObject.tag == "DaTroi")
        {
            if(coll.gameObject.GetComponent<datroicontroller>().checkEnd == false)
            {
                checkEnterDaTroi = true;
                checkJump = false;
                boxPlayer.isTrigger = false;
                coll.gameObject.GetComponent<datroicontroller>().checkEnterPlayer = true;
                speedMovePlayer = coll.gameObject.GetComponent<datroicontroller>().speedMove;
                if (numberPlayer == 1)
                {
                    anim.Play("standMonkey");
                }
                if (numberPlayer == 2)
                {
                    anim.Play("standPeople");
                }
            }
            
        }


        //enter ho nuoc
        if (coll.gameObject.tag == "MapHoNuoc")
        {
            PlayerDie();
        }
        if(coll.gameObject.tag == "Frog" && itemBaove == true)
        {
            coll.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        }
        //end level
        if (coll.gameObject.tag == "EndLevel")
        {
            sceneGameController.victoryPopup.SetActive(true);
            Time.timeScale = 0;
            speedMovePlayer = 0;
        }
        if(coll.gameObject.tag == "PlatformFly")
        {
            checkJump = false;
            PlayerWalk();
        }
    }

    private void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "DaTroi")
        {
            checkEnterDaTroi = false;
            speedMovePlayer = speedMove;
        }

    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
       
        //enter swamp
        if (coll.gameObject.tag == "Swamp")
        {
            speedMovePlayer = speedMoveSwamp;
        }
        //enter destroy box
        if (coll.gameObject.tag == "Box")
        {
            speedMovePlayer = speedMoveBox;
            coll.GetComponent<BoxController>().BoxDie();
        }
        // enter destroy nhen

        if (coll.gameObject.tag == "Nhen")
        {
            coll.GetComponent<NhenController>().NhenDie();
        }

        // enter bee
        if (coll.gameObject.tag == "Bee" && itemBaove == false)
        {
            PlayerDie();
        }

        //enterGold
        if (coll.gameObject.tag == "gold")
        {
            coll.GetComponent<GoldController>().EneterPlayer();
            sceneGameController.score += 1;
        }

        if (coll.gameObject.tag == "Baove")
        {
            StartCoroutine(Baove());
            Destroy(coll.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Swamp")
        {
            speedMovePlayer = speedMove;
        }
    }
    private IEnumerator Baove()
    {
        baoVe.SetActive(true);
        itemBaove = true;
        yield return new WaitForSeconds(timeWaitBaove);
        itemBaove = false;
        baoVe.SetActive(false);
    }

    void PlayerDie()
    {
        checkDie = true;
        boxPlayer.isTrigger = true;
        if (numberPlayer == 1)
        {
            anim.Play("dieMonkey");
        }
        if (numberPlayer == 2)
        {
            anim.Play("diePeople");
        }
        audioController.backGroundSource.Stop();
        audioController.PlayAudio(audioController.audioDie);
        if(sceneGameController.idLevel> PlayerPrefs.GetInt("idLevel"))
        {
            PlayerPrefs.SetInt("idLevel", sceneGameController.idLevel);
            PlayerPrefs.Save();
        }
        if (sceneGameController.score > PlayerPrefs.GetFloat("highscore"))
        {
            PlayerPrefs.SetFloat("highscore", sceneGameController.score);
            PlayerPrefs.Save();
        }
        sceneGameController.scoreTextGameOver.text = "Gold : " + sceneGameController.score.ToString();
        sceneGameController.hightscoreText.text = "High Gold : " + PlayerPrefs.GetFloat("highscore").ToString();
        gameObject.SetActive(false);
        sceneGameController.gameoverPopup.SetActive(true);
        sceneGameController.checkPause = false;
        transform.Translate(new Vector3(-1, 1, 0) * Time.deltaTime * speedDie);
    }
    void PlayerWalk()
    {
        if (numberPlayer == 1)
        {
            anim.Play("walkMonkey");
        }
        if (numberPlayer == 2)
        {
            anim.Play("walkPeople");
        }
    }
    void PlayerJump()
    {
        if (numberPlayer == 1)
        {
            anim.Play("jumpMonkey");
        }
        if (numberPlayer == 2)
        {
            anim.Play("jumpPeople");
        }
        audioController.PlayAudio(audioController.audioJump);
        rigid.AddForce(new Vector3(0, 1, 0) * speedJump * 100);
        checkJump = true;
    }
}
