﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneGameController : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [SerializeField] AudioController audioController;

    private SceneGameController instance;

    public GameObject playPopup;
    public GameObject mapPopup;
    public GameObject pausePopup;
    public GameObject gameoverPopup;
    public float score;
    public GameObject scoreTextView;
    public GameObject pause;
    public GameObject settingPopup;
    public GameObject victoryPopup;
    //public GameObject LoadLevel;

    public GameObject player;
    public GameObject backGround;
    public Text scoreText;
    public Text scoreTextGameOver;
    public Text hightscoreText;

    public bool checkPause;

    public int idLevel;

    // Use this for initialization
    private void Start()
    {
        playerController.numberPlayer = 1;
        victoryPopup.SetActive(false);
        settingPopup.SetActive(false);
        player.SetActive(false);
        checkPause = true;
        pausePopup.SetActive(false);
        gameoverPopup.SetActive(false);
        scoreTextView.SetActive(false);
        pause.SetActive(false);
        idLevel = PlayerPrefs.GetInt("idLevel");
        backGround.SetActive(false);
        playPopup.SetActive(true);
        mapPopup.SetActive(false);
        score = 0;
        player.transform.position = new Vector2(-3, -2);
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        scoreText.text = score.ToString();

    }
    //play
    public void ButtonPlay()
    {
        playPopup.SetActive(false);
        mapPopup.SetActive(true);
    }
    //level1
    public void ButtonLevel1()
    {
        SceneManager.LoadSceneAsync("Level1");
        idLevel = 1;
        LoadLevel();
    }
    //level2
    public void ButtonLevel2()
    {
        SceneManager.LoadSceneAsync("Level2");
        idLevel = 2;
        LoadLevel();
    }
    //pause
    public void ButtonPause()
    {
        if (checkPause == true)
        {
            Time.timeScale = 0;
            pausePopup.SetActive(true);
            playerController.speedMovePlayer = 0;
        }

    }
    //next level
    public void ButtonNextLevel()
    {
        if(idLevel<=PlayerPrefs.GetInt("idLevel"))
        {
            SceneManager.LoadScene("Level" + (idLevel + 1));
            idLevel = idLevel + 1;
            victoryPopup.SetActive(false);
            playerController.speedMovePlayer = playerController.speedMove;
            Time.timeScale = 1;
            player.SetActive(true);
            checkPause = true;
            gameoverPopup.SetActive(false);
            pausePopup.SetActive(false);
            audioController.backGroundSource.Play();
            player.transform.position = new Vector2(-3, -2);
        }
        else
        {
            SceneManager.LoadScene("Level" + PlayerPrefs.GetInt("idLevel"));
            victoryPopup.SetActive(false);
            playerController.speedMovePlayer = playerController.speedMove;
            Time.timeScale = 1;
            player.SetActive(true);
            checkPause = true;
            gameoverPopup.SetActive(false);
            pausePopup.SetActive(false);
            audioController.backGroundSource.Play();
            player.transform.position = new Vector2(-3, -2);
        }
        
    }
    //menu
    public void ButtonMenu()
    {
        player.SetActive(false);
        checkPause = true;
        pausePopup.SetActive(false);
        gameoverPopup.SetActive(false);
        scoreTextView.SetActive(false);
        pause.SetActive(false);
        backGround.SetActive(false);
        playPopup.SetActive(true);
        mapPopup.SetActive(false);
        checkPause = true;
        victoryPopup.SetActive(false);
    }
    //restart
    public void ButtonRestart()
    {
        SceneManager.LoadScene("Level" + idLevel);
        playerController.speedMovePlayer = playerController.speedMove;
        Time.timeScale = 1;
        player.SetActive(true);
        checkPause = true;
        gameoverPopup.SetActive(false);
        pausePopup.SetActive(false);
        score = 0;
        audioController.backGroundSource.Play();
        player.transform.position = new Vector2(-3, -2);
    }
    //resume
    public void ButtonResume()
    {
        Time.timeScale = 1;
        playerController.speedMovePlayer = playerController.speedMove;
        pausePopup.SetActive(false);
    }
    //quit
    public void ButtonQuit()
    {
        Application.Quit();
    }
    //setting
    public void ButtonSetting()
    {
        settingPopup.SetActive(true);
    }

    //edit
    public void ButtonEditSetting()
    {
        settingPopup.SetActive(false);
    }
    //monkey
    public void ButtonPickMonkey()
    {
        settingPopup.SetActive(false);
        PlayerPrefs.SetFloat("NumberPlayer", playerController.numberPlayer = 1);
        PlayerPrefs.Save();
    }
    //people
    public void ButtonPickPeople()
    {
        settingPopup.SetActive(false);
        PlayerPrefs.SetFloat("NumberPlayer", playerController.numberPlayer = 2);
        PlayerPrefs.Save();
    }
    //play poup
    public void ButtonBackPlayPopup()
    {
        mapPopup.SetActive(false);
        playPopup.SetActive(true);
    }
    //load level
    void LoadLevel()
    {
        mapPopup.SetActive(false);
        backGround.SetActive(true);
        player.SetActive(true);
        scoreTextView.SetActive(true);
        pause.SetActive(true);
        gameoverPopup.SetActive(false);
        score = 0;
        Time.timeScale = 1;
        audioController.backGroundSource.Play();
        transform.position = new Vector2(-3, -2);
    }
}
